# query-params

Chrome extension to quickly edit a url's query parameters

## Installation Instructions

1. Clone this repo locally
2. Navigate to chrome://extensions/
3. Make sure the `Developer Mode` toggle is on
4. Click `Load Unpacked`
5. Choose the directory you cloned the repo into
