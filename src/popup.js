let clipboardBtn = document.getElementById("clipboard");
let applyBtn = document.getElementById("apply");
let saveBtn = document.getElementById("save");
let deleteBtn = document.getElementById("delete");
let presetNameInput = document.getElementById("preset-name");
let presetsList = document.getElementById("presets-list");
let addBtn = document.getElementById("add");
let helpBtn = document.getElementById("help");
let parametersTable = document.getElementById("parameters-table");

let createParametersRow = (id = "", value = "") => {
  let row = document.createElement("tr");

  let idCol = document.createElement("td");
  let idInput = document.createElement("input");
  idInput.value = id;
  idCol.appendChild(idInput);
  row.appendChild(idCol);

  let valueCol = document.createElement("td");
  let valueInput = document.createElement("input");
  valueInput.value = value;
  valueCol.appendChild(valueInput);
  row.appendChild(valueCol);

  let delCol = document.createElement("td");
  let delBtn = document.createElement("button");
  delBtn.innerHTML = "✖️";
  delBtn.onclick = (element) => {
    row.parentNode.removeChild(row);
  };
  delCol.appendChild(delBtn);
  row.appendChild(delCol);

  return row;
};

let loadPresets = () => {
  presetsList.innerHTML = "";
  chrome.storage.sync.get("presets", function (data) {
    let presets = data.presets;
    let folders = {};
    Object.keys(presets).forEach((fullKey) => {
      let btn = document.createElement("button");
      let folder = "";
      let key;
      let slashIndex = fullKey.indexOf("/");
      if (slashIndex !== -1) {
        folder = fullKey.substr(0, slashIndex);
        key = fullKey.substr(slashIndex + 1);
      } else {
        key = fullKey;
      }
      btn.innerHTML = key;
      btn.onclick = loadPreset(fullKey, presets[fullKey], btn);
      btn.oncontextmenu = combinePreset(fullKey, presets[fullKey], btn);
      if (folder in folders) {
        folders[folder].push(btn);
      } else {
        folders[folder] = [btn];
      }
    });

    Object.keys(folders)
      .sort()
      .forEach((folder) => {
        let section = document.createElement("section");
        let header = document.createElement("h3");
        header.innerHTML = folder;
        presetsList.appendChild(header);

        folders[folder].forEach((btn) => {
          section.appendChild(btn);
        });
        presetsList.appendChild(section);
      });
  });
};

loadPresets();

let loadPreset = (key, preset, btn) => {
  return (event) => {
    [...presetsList.getElementsByTagName("*")].forEach(
      (el) => (el.className = "")
    );
    btn.className = "active";

    presetNameInput.value = key;

    loadParamsIntoTable(preset);
  };
};

let combinePreset = (key, preset, btn) => {
  return (event) => {
    btn.className = "active";

    let params = readTableToMap();
    params = { ...params, ...preset };

    loadParamsIntoTable(params);
    return false;
  };
};

function createUrl(tabs) {
  let newUrl = tabs[0].url.split("?")[0];

  let rows = parametersTable.children;
  for (let i = 0; i < rows.length; i++) {
    newUrl +=
      (i ? "&" : "?") +
      rows[i].children[0].children[0].value +
      "=" +
      rows[i].children[1].children[0].value;
  }

  return newUrl;
}

clipboardBtn.onclick = function (element) {
  chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
    let copyFrom = document.createElement("textarea");
    copyFrom.textContent = createUrl(tabs);
    document.body.appendChild(copyFrom);
    copyFrom.select();
    document.execCommand("copy");
    copyFrom.blur();
    document.body.removeChild(copyFrom);
  });
};

applyBtn.onclick = function (element) {
  chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
    chrome.tabs.update(tabs[0].id, { url: createUrl(tabs) });
  });
};

saveBtn.onclick = function (element) {
  chrome.storage.sync.get("presets", function (data) {
    let newPresets = data.presets ? data.presets : {};
    let newPresetName = presetNameInput.value;

    if (newPresets[newPresetName]) {
      var result = confirm(
        `Do you want to overwrite the "${newPresetName}" preset`
      );
      if (!result) {
        return;
      }
    }

    let params = readTableToMap();
    newPresets[presetNameInput.value || "untitled"] = params;

    chrome.storage.sync.set({ presets: newPresets }, function () {
      loadPresets();
    });
  });
};

let readTableToMap = function () {
  let rows = parametersTable.children;
  let params = {};
  for (let i = 0; i < rows.length; i++) {
    params[rows[i].children[0].children[0].value] =
      rows[i].children[1].children[0].value;
  }
  return params;
};

let loadParamsIntoTable = function (params) {
  parametersTable.innerHTML = "";
  for (let key in params) {
    parametersTable.appendChild(createParametersRow(key, params[key]));
  }
};

addBtn.onclick = function (element) {
  parametersTable.appendChild(createParametersRow());
};

helpBtn.onclick = function (element) {
  alert(`Version 1.0.0

Right click multiple presets to combine them
Use a single '/' in preset names to organize your presets into sections`);
};

deleteBtn.onclick = function (element) {
  chrome.storage.sync.get("presets", function (data) {
    let newPresets = data.presets ? data.presets : {};
    delete newPresets[presetNameInput.value];

    chrome.storage.sync.set({ presets: newPresets }, function () {
      loadPresets();
    });
  });
};

chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
  let queryString = /^[^#?]*(\?[^#]+|)/.exec(tabs[0].url)[1];

  if (queryString.length > 0) {
    let queriesArray = queryString.slice(1).split("&");

    for (let i = 0; i < queriesArray.length; i++) {
      let [qid, qvalue] = queriesArray[i].split("=");
      parametersTable.appendChild(createParametersRow(qid, qvalue));
    }
  }
});
